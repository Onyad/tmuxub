#pragma once

#include <filesystem>
#include <optional>
#include <string>

#if !defined(__linux__) && !defined(__APPLE__)
#error Only linux and Macos supported
#endif

namespace tmuxub {
class Tmux {
 public:
  Tmux(std::optional<std::filesystem::path> socket = std::nullopt) : socket_(socket) {}

  bool HasSession(const std::string& name);

  Tmux& ConnectToSession(const std::string& name);
  Tmux& CreateOrConnect(const std::string& name);
  Tmux& CreateSession(const std::string& name);
  Tmux& LockSession();
  Tmux& KillSession();

  void Exec();

  Tmux& CreateWindow(const std::string& window);

  // | vertical, - horizontal, > move to next window, {cmd1 cmd2; cmd3}
  // cmd3 in new batch, for example hotkey in nvim must send after nvim start
  // . select current pane
  //
  // |-{ls}>|{pwd}
  // +-------+-------+
  // |ls     |       |
  // |       |       |
  // +---+---+       |
  // |pwd|   |       |
  // |   |   |       |
  // +---+---+-------+
  Tmux& SplitWindow(const std::string& pattern);

 private:
  void ConnectToSession_(const std::string& name) {
    session_ = name;
  }

  std::string GetCommandTmuxSocket() {
    return std::string("tmux ") + (socket_ ? "-S " + std::string(socket_.value()) + " " : "");
  }

 private:
  std::optional<std::filesystem::path> socket_;
  std::optional<std::string> session_;
};
}  // namespace tmuxub
