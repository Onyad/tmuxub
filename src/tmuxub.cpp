#include <tmuxub/tmuxub.hpp>
#include <iostream>

namespace tmuxub {
bool Tmux::HasSession(const std::string& name) {
  std::string command = GetCommandTmuxSocket() + "has-session -t " + name;

  int x = system(command.data());

  return x == 0;
}

Tmux& Tmux::ConnectToSession(const std::string& name) {
  if (!HasSession(name)) {
    throw std::logic_error("hasn't session " + name);
  }
  session_ = name;

  LockSession();

  return *this;
}

Tmux& Tmux::CreateOrConnect(const std::string& name) {
  if (HasSession(name)) {
    std::cout << "I find session, connect" << std::endl;
    ConnectToSession_(name);
  } else {
    std::cout << "I not find session, create" << std::endl;
    CreateSession(name);
  }

  return *this;
}

Tmux& Tmux::CreateSession(const std::string& name) {
  std::string command = GetCommandTmuxSocket() + "new-session -d -s " + name;

  if (system(command.data()) != 0) {
    throw std::logic_error("couldn't create session");
  }

  session_ = name;

  LockSession();

  return *this;
}

Tmux& Tmux::LockSession() {
  std::string command = GetCommandTmuxSocket() + "lock-session -t " + session_.value();

  if (system(command.data()) != 0) {
    throw std::logic_error("couldn't lock session");
  }

  return *this;
}

Tmux& Tmux::SplitWindow(const std::string& pattern) {
  size_t number_pane = 0;
  auto split_horizontal = [this, &number_pane]() {
    std::string command =
        GetCommandTmuxSocket() + "split-window -t " + "0." + std::to_string(number_pane);

    system(command.data());
  };
  auto split_vertical = [this, &number_pane]() {
    std::string command =
        GetCommandTmuxSocket() + "split-window -h -t " + "0." + std::to_string(number_pane);

    system(command.data());
  };
  auto send_cmd = [this, &number_pane](const std::string& cmd) {
    std::string command =
        GetCommandTmuxSocket() + "send-keys -t " + "0." + std::to_string(number_pane) + " " + cmd;

    system(command.data());
  };
  auto select_current_pane = [this, &number_pane](){
    std::string command =
        GetCommandTmuxSocket() + "select-pane -t " + "0." + std::to_string(number_pane);

    system(command.data());
  };

  bool is_cmd = false;
  std::string cmd;
  for (char x : pattern) {
    if (is_cmd) {
      if (x == '}') {
        is_cmd = false;
        send_cmd(cmd);
      } else if (x == ';') {
        send_cmd(cmd);
        cmd.clear();
      } else {
        cmd += x;
      }
      continue;
    }
    switch (x) {
      case '|':
        split_vertical();
        break;
      case '-':
        split_horizontal();
        break;
      case '>':
        number_pane++;
        break;
      case '<':
        if (number_pane == 0) {
          throw std::logic_error("Number pane connot be less than zero");
        }
        number_pane--;
        break;
      case '{':
        is_cmd = true;
        cmd.clear();
        break;
      case '.':
        select_current_pane();
        break;
    }
  }

  return *this;
}

void Tmux::Exec() {
  std::string command = this->GetCommandTmuxSocket() + "attach -t " + session_.value();
  if (system(command.data()) != 0) {
    throw std::logic_error("exec");
  }
}

Tmux& Tmux::KillSession() {
  std::string command = std::string("tmux ") +
                        (socket_ ? "-S " + std::string(socket_.value()) + " " : "") +
                        "kill-session -t " + session_.value();

  if (system(command.data()) != 0) {
    throw std::logic_error("couldn't kill session");
  }

  return *this;
}
}  // namespace tmuxub
